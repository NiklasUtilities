#!/usr/bin/perl

# Generate the blockMeshDict for the contracting pipe flow
# for the ERCOFTAC Case UFR 4-14.
# Underlying Flow Regime 4-14. Confined Flows
# By: Niklas Nordin (niklas.nordin@nequam.se)
# Date: 22 April 2010.

use strict;

my $nArgs = $#ARGV;

if ($nArgs != 7)
{
    usage();
    exit;
}
my @input = @ARGV;
my $L = $ARGV[0];
my $l = $ARGV[1];
my $D = $ARGV[2];
my $sigma = $ARGV[3];
my $nx1 = $ARGV[4];
my $nx2 = $ARGV[5];
my $ny = $ARGV[6];
my $ny2 = $ARGV[7];

my $d = sqrt($sigma)*$D;

my $angle = 3.14159265358979323*45.0/180.0;
my $r0 = 0.5*$d;
my $R0 = 0.5*$D;
my $r = 0.5*$d*cos($angle);
my $R = 0.5*$D*cos($angle);
my $q = 0.7*$r;
my $h = $l + $L;

my $ql = 0.7*$R;

my $g = 5.0;
my $g2 = 5.0;
my $g3 = 10.0;
my $g4 = 10.0;

my $gi = 1.0/$g;
my $g2i = 1.0/$g2;
my $g3i = 1.0/$g3;
my $g4i = 1.0/$g4;

printHeader("2.0");
printConvertToMeters("1.0");
printVertices();
printBlocks();
printEdges();
printPatches();

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

sub printHeader
{
    print "FoamFile\n";
    print "{\n";
    print "    version\t $_[0];\n";
    print "    format\t ascii;\n";
    print "    class\t dictionary;\n";
    print "    object\t blockMeshDict;\n";
    print "}\n\n";
    print "/*\n";
    print "Automatically generated with $0 @input\n";
    print "*/\n\n";
}

sub printConvertToMeters
{
    print "convertToMeters\t $_[0];\n\n";
}

sub printVertices
{
    print "vertices\n";
    print "(\n";
    print "    ( $h -$q -$q )\n";
    print "    ( $h  $q -$q )\n";
    print "    ( $h -$q  $q )\n";
    print "    ( $h  $q  $q )\n";

    print "    ( $h -$r -$r )\n";
    print "    ( $h  $r -$r )\n";
    print "    ( $h -$r  $r )\n";
    print "    ( $h  $r  $r )\n";

    print "    ( $L -$q -$q )\n";
    print "    ( $L  $q -$q )\n";
    print "    ( $L -$q  $q )\n";
    print "    ( $L  $q  $q )\n";

    print "    ( $L -$r -$r )\n";
    print "    ( $L  $r -$r )\n";
    print "    ( $L -$r  $r )\n";
    print "    ( $L  $r  $r )\n";

    print "    ( 0  -$q -$q )\n";
    print "    ( 0   $q -$q )\n";
    print "    ( 0  -$q  $q )\n";
    print "    ( 0   $q  $q )\n";

    print "    ( 0  -$ql -$ql )\n";
    print "    ( 0   $ql -$ql )\n";
    print "    ( 0  -$ql  $ql )\n";
    print "    ( 0   $ql  $ql )\n";

    print "    ( $L -$R -$R )\n";
    print "    ( $L  $R -$R )\n";
    print "    ( $L -$R  $R )\n";
    print "    ( $L  $R  $R )\n";

    print "    ( 0  -$R -$R )\n";
    print "    ( 0   $R -$R )\n";
    print "    ( 0  -$R  $R )\n";
    print "    ( 0   $R  $R )\n";
    print ");\n\n";
}

sub printBlocks
{
    print "blocks\n";
    print "(\n";
    print "    hex (8 0 1 9 10 2 3 11) ($nx1 $ny $ny) simpleGrading ($g4 1 1)\n";
    print "    hex (16 8 9 17 18 10 11 19) ($nx2 $ny $ny) simpleGrading ($g3i 1 1)\n";

    print "    hex (12 4 5 13 8 0 1 9) ($nx1 $ny $ny) simpleGrading ($g4 1 $g2)\n";
    print "    hex (20 12 13 21 16 8 9 17) ($nx2 $ny $ny) simpleGrading ($g3i 1 $g2)\n";

    print "    hex (12 4 0 8 14 6 2 10) ($nx1 $ny $ny) simpleGrading ($g4 $g2 1)\n";
    print "    hex (20 12 8 16 22 14 10 18) ($nx2 $ny $ny) simpleGrading ($g3i $g2 1)\n";

    print "    hex (9 1 5 13 11 3 7 15) ($nx1 $ny $ny) simpleGrading ($g4 $g2i 1)\n";
    print "    hex (17 9 13 21 19 11 15 23) ($nx2 $ny $ny) simpleGrading ($g3i $g2i 1)\n";

    print "    hex (10 2 3 11 14 6 7 15) ($nx1 $ny $ny) simpleGrading ($g4 1 $g2i)\n";
    print "    hex (18 10 11 19 22 14 15 23) ($nx2 $ny $ny) simpleGrading ($g3i 1 $g2i)\n";

    print "    hex (28 24 25 29 20 12 13 21) ($nx2 $ny $ny2) simpleGrading ($g3i 1 $g)\n";
    print "    hex (28 24 12 20 30 26 14 22) ($nx2 $ny2 $ny) simpleGrading ($g3i $g 1)\n";
    print "    hex (22 14 15 23 30 26 27 31) ($nx2 $ny $ny2) simpleGrading ($g3i 1 $gi)\n";
    print "    hex (21 13 25 29 23 15 27 31) ($nx2 $ny2 $ny) simpleGrading ($g3i $gi 1)\n";
    print ");\n\n";
}

sub printEdges
{
    print "edges\n";
    print "(\n";
    print "    arc 4 5 ($h 0 -$r0)\n";
    print "    arc 5 7 ($h $r0 0)\n";
    print "    arc 6 7 ($h 0 $r0)\n";
    print "    arc 4 6 ($h -$r0 0)\n";

    print "    arc 12 13 ($L 0 -$r0)\n";
    print "    arc 13 15 ($L $r0 0)\n";
    print "    arc 14 15 ($L 0 $r0)\n";
    print "    arc 12 14 ($L -$r0 0)\n";

    print "    arc 24 25 ($L 0 -$R0)\n";
    print "    arc 25 27 ($L $R0 0)\n";
    print "    arc 26 27 ($L 0 $R0)\n";
    print "    arc 24 26 ($L -$R0 0)\n";

    print "    arc 28 29 (0 0 -$R0)\n";
    print "    arc 29 31 (0 $R0 0)\n";
    print "    arc 30 31 (0 0 $R0)\n";
    print "    arc 28 30 (0 -$R0 0)\n";

    print ");\n\n";
}

sub printPatches
{
    print "patches\n";
    print "(\n";
    print "    wall walls\n";
    print "    (\n";
    print "        (12 13 5 4)\n";
    print "        (13 15 7 5)\n";
    print "        (15 14 6 7)\n";
    print "        (14 12 4 6)\n";
    print "        (13 12 24 25)\n";
    print "        (27 15 13 25)\n";
    print "        (27 26 14 15)\n";
    print "        (26 24 12 14)\n";
    print "        (31 30 26 27)\n";
    print "        (29 31 27 25)\n";
    print "        (28 29 25 24)\n";
    print "        (30 28 24 26)\n";
    print "    )\n";

    print "    patch inlet\n";
    print "    (\n";
    print "        (18 19 17 16)\n";
    print "        (16 17 21 20)\n";
    print "        (22 18 16 20)\n";
    print "        (19 23 21 17)\n";
    print "        (22 23 19 18)\n";
    print "        (20 21 29 28)\n";
    print "        (30 22 20 28)\n";
    print "        (23 31 29 21)\n";
    print "        (30 31 23 22)\n";
    print "    )\n";

    print "    patch outlet\n";
    print "    (\n";
    print "        (7 6 2 3)\n";
    print "        (3 2 0 1)\n";
    print "        (2 6 4 0)\n";
    print "        (1 0 4 5)\n";
    print "        (7 3 1 5)\n";
    print "    )\n";
    print ");\n\n";
}

sub usage
{
    print "usage:\n";
    print "\t $0 <L> <l> <D> <sigma> <nx1> <nx2> <ny> <ny2>\n";
    print "\t where\n\t L = length of the large diameter tube.\n";
    print "\t l = length of the small diameter tube.\n";
    print "\t D = large tube diameter.\n";
    print "\t sigma = Area ratio, d^2/D^2.\n";
    print "\t nx = number of cells in flow direction for small tube.\n";
    print "\t nx2 = number of cells in flow direction for large tube.\n";
    print "\t ny = number of cells in square of small tube.\n";
    print "\t ny2 = number of cells in outside area of large tube.\n";
}
