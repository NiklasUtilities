    Info<< "Reading pdfProperties\n" << endl;

    IOdictionary pdfProperties
    (
        IOobject
        (
            "pdfProperties",
            runTime.constant(),
            runTime,
            IOobject::MUST_READ,
            IOobject::NO_WRITE
        )
    );

    Random rndGen(label(0));

    label nSamples(readLabel(pdfProperties.lookup("nSamples")));

    autoPtr<pdf> myPDF
    (
        pdf::New
        (
            pdfProperties.subDict("dropletPDF"),
            rndGen
        )

    );

    word fileName(pdfProperties.lookup("fileName"));

    OFstream pdfFile
    (
        fileName
    );
