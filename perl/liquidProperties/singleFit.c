/*
    icc -O2 -mp -prec_div -pc80 -o singleFit singleFit.c

*/

#include <math.h>
#include <stdio.h>

double eq100(double T, double a, double b, double c, double d, double e)
{
    double y = (((e*T + d)*T + c)*T + b)*T + a;
    return y;
}

double eq101(double T, double a, double b, double c, double d, double e)
{
    double ev = a + b/T + c*log(T) + d*pow(T,e);
    double y = exp(ev);
    return y;
}

double eq102(double T, double a, double b, double c, double d, double e)
{
    double y = a*pow(T,b)/(1.0 + c/T + d/T/T);
    return y;
}

double eq103(double T, double a, double b, double c, double d, double e)
{
    double y = a + b*exp(-c/pow(T,d));
    return y;
}

double eq104(double T, double a, double b, double c, double d, double e)
{
    double y = a + b/T + c/pow(T, 3) + d/pow(T, 8) + e/pow(T, 9);
    return y;
}


double eq105(double T, double a, double b, double c, double d, double e)
{
    double y = a/pow(b, 1.0 + pow(1.0 - T/c, d));
    return y;
}
/*
double min(double a, double b)
{
    double m = a;
    if (b < m)
    {
        m = b;
    }
    
    return m;
}

double max(double a, double b)
{
    double m = a;
    if (b > m)
    {
        m = b;
    }
    
    return m;
}
    */

double dabs(double v)
{
    double a = v;
    if (a < 0)
    {
        a = -v;
    }
    return a;
}

main()
{
    int N = 2, nPoints = 100;
    double err = 1.0e-5, delta = 1.0e-6;

    double rx = 4.0e-3;

    double Tmin, Tmax;
    double a[N], b[N], c[N], d[N], e[N], p[N];
    double as, bs, cs, ds, es;
    double aMin, bMin, cMin, dMin, eMin;
    double aMax, bMax, cMax, dMax, eMax;
    double Y[nPoints];

    Tmin = 244.0;
    Tmax = 617.0;

    // alphaMethylNapthalene
    int i = 0;
    a[i] = 60.92559;
    b[i] = 0.22408;
    c[i] = 772.04; 
    d[i] = 0.25709;
    e[i] = 0;
    
    // n-Decane
    i = 1;
    a[i] = 60.94208835;
    b[i] = 0.25745; 
    c[i] = 617.7;
    d[i] = 0.28912;
    e[i] = 0;

    // IDEA fuel 
    // 30 % alphaMethylNaphtalene & 70 % nDecane
    p[0] = 0.3;
    p[1] = 0.7;

    // initial guess for coefficients
    for(int i = 0; i<N; i++)
    {
        as += p[i]*a[i];
        bs += p[i]*b[i];
        cs += p[i]*c[i];
        ds += p[i]*d[i];
        es += p[i]*e[i];
    }

    // pretty good initial guess
    as = 75.58;
    bs = 0.2763;
    cs = 625.89;
    ds = 0.25628;
    es = 0.0;

    // calculate the blended function at the given T's
    // and store the values in Y[]
    for(int i = 0; i < nPoints; i++)
    {
        double T = Tmin + (Tmax - Tmin)*i/(nPoints - 1.0);

        double fSum = 0.0;
        for(int j=0; j < N; j++)
        {
            fSum += p[j]*eq105(T, a[j], b[j], c[j], d[j], e[j]);
        }
        Y[i] = fSum;
    }

    // iterate necesse est
    double elof = 1;
    int iter = 0;
    double prevSum = -1.0e+10;

    double T;
    double eq0, eqam,eqap, eqbm,eqbp, eqcm,eqcp, eqdm,eqdp, eqem,eqep;
    double dda, ddb, ddc, ddd, dde;
    double dera, derb, derc, derd, dere;
    double dera0, derb0, derc0, derd0, dere0;
    double a0,a1, b0,b1, c0,c1, d0,d1, e0,e1;

    while (dabs(elof) > err)
    {
        iter++;
        dera = 0.0;
        derb = 0.0;
        derc = 0.0;
        derd = 0.0;
        dere = 0.0;

        double sum = 0;

        for(int i = 0; i < nPoints; i++)
        {            
            T = Tmin + (Tmax - Tmin)*i/(nPoints - 1.0);

            eq0 = eq105(T, as, bs, cs, ds, es);

            eqam = eq105(T, as-delta, bs, cs, ds, es);
            eqap = eq105(T, as+delta, bs, cs, ds, es);
            eqbm = eq105(T, as, bs-delta, cs, ds, es);
            eqbp = eq105(T, as, bs+delta, cs, ds, es);
            eqcm = eq105(T, as, bs, cs-delta, ds, es);
            eqcp = eq105(T, as, bs, cs+delta, ds, es);
            eqdm = eq105(T, as, bs, cs, ds-delta, es);
            eqdp = eq105(T, as, bs, cs, ds+delta, es);
            eqem = eq105(T, as, bs, cs, ds, es-delta);
            eqep = eq105(T, as, bs, cs, ds, es+delta);
            
            double diff = Y[i] - eq0;
            double diff2 = diff*diff;

            a1 = pow(Y[i] - eqap, 2);
            a0 = pow(Y[i] - eqam, 2);
            dera += 0.5*(a1 - a0)/delta;

            b1 = pow(Y[i] - eqbp, 2);
            b0 = pow(Y[i] - eqbm, 2);
            derb += 0.5*(b1 - b0)/delta;

            c1 = pow(Y[i] - eqcp, 2);
            c0 = pow(Y[i] - eqcm, 2);
            derc += 0.5*(c1 - c0)/delta;

            d1 = pow(Y[i] - eqdp, 2);
            d0 = pow(Y[i] - eqdm, 2);
            derd += 0.5*(d1 - d0)/delta;

            e1 = pow(Y[i] - eqep, 2);
            e0 = pow(Y[i] - eqem, 2);
            dere += 0.5*(e1 - e0)/delta;

            sum += diff2;
        }
        elof = dabs(dera) + dabs(derb) + dabs(derc) + dabs(derd) + dabs(dere);
        //elof = 0;

        if (!(iter % 10000))
        {
            printf("iter=%i, err=%10.3e, sum=%10.3e, da=%10.3e, db=%10.3e, dc=%10.3e, dd=%10.3e, de=%10.3e.\n", iter, elof, sum, dera, derb, derc, derd, dere);
            printf("a=%14.7f, b=%14.7f, c=%14.7f, d=%14.7f, e=%14.7f.\n", as, bs, cs, ds, es);
        }

        double step = delta*rx;

        as -= dera*step;
        bs -= derb*step;
        cs -= derc*step;
        ds -= derd*step;
        es -= dere*step;

    }

    printf("a = %f, b = %f, c = %f, d = %f, e = %f.\n", as, bs, cs, ds, es);
}
