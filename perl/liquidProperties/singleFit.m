clear all;
format long;

% ****************************************************
% Number of single component fuels
% ****************************************************
N = 2;

% ****************************************************
% Equation to be used in the least square fit
% ****************************************************
eq = 105;

Tmin = 244.0;
Tmax = 617.0;
nPoints = 100;

err = 1.0e-5;
delta = 1.0e-9;
rx = 0.2;

% alphaMethylNapthalene
i = 1;
% rho
Tcrit(i) = 772.04;
a(i) = 60.92559;
b(i) = 0.22408;
c(i) = 772.04; 
d(i) = 0.25709;
e(i) = 0;

% pv
a(i) = 112.73;
b(i) = -9749.6;
c(i) = -13.245;
d(i) = 7.1266e-06;
e(i) = 2.0;

% cp
%a(i) = 965.893108298172;
%b(i) = 1.16216596343179;
%c(i) = 0.00298523206751055;
%d(i) = 0.0;
%e(i) = 0.0;

% hl
a(i) = 511744.022503516;
b(i) = 0.4164;
c(i) = 0.0;
d(i) = 0.0;
e(i) = 0.0;

% n-Decane
i = 2;
Tcrit(i) = 617.70;
% rho
a(i) = 60.94208835;
b(i) = 0.25745; 
c(i) = 617.7;
d(i) = 0.28912;
e(i) = 0;
% pv
a(i) = 73.716;
b(i) = -9103.2;
c(i) = -7.2253;
d(i) = 2.062e-06;
e(i) = 2.0;

% cp
%a(i) = 1958.18252099659;
%b(i) = -1.39094071757388;
%c(i) = 0.00754612221948905;
%d(i) = 0.0;
%e(i) = 0.0;

% hl
a(i) = 464743.296904101;
b(i) = 0.39797;
c(i) = 0.0;
d(i) = 0.0;
e(i) = 0.0;

p = [0.3, 0.7];
% initial start guess for the coefficients

Tcrits = 618.074;
as = 0.0;
bs = 0.0;
cs = 0.0;
ds = 0.0;
es = 0.0;
for n=1:N,
        as = as + p(n)*a(i);
bs = bs + p(n)*b(i);
cs = cs + p(n)*c(i);
ds = ds + p(n)*d(i);
es = es + p(n)*e(i);
end;

% initial start guess for the coefficients
as = 4.7884352e+05;
bs = 7.70531154e-1;
cs = -1.936846;
ds = 3.051527;
es = -1.597438143;

% calculate the blended function at the given temperatures
for i = 1:nPoints,
          T(i) = Tmin + (Tmax-Tmin)*(i-1)/(nPoints-1);
fSum = 0;
for n=1:N,
        eq = eq106(T(i)/Tcrit(n), a(n), b(n), c(n), d(n), e(n));
fSum = fSum + p(n)*eq;
end
Y(i) = fSum;
Y1(i) = eq106(T(i)/Tcrit(1), a(1), b(1), c(1), d(1), e(1));
Y2(i) = eq106(T(i)/Tcrit(2), a(2), b(2), c(2), d(2), e(2));
Ys(i) = eq106(T(i)/Tcrits, as, bs, cs, ds, es);
end

%plot(T,Y1,T,Y2,T,Y,T,Ys,'o');
plot(T,Y,'ko-');

%legend('\alpha-MethylNaphthalene','n-Decane','IDEA', 'least sq. fit');
axis tight
grid on;

xlabel('Temperature');
ylabel('Surface Tension');
axis([250 617 0 4.0e5]);
