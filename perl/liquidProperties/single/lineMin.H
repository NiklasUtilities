eqArray cRef(IDEA.coeffs());
eqArray c0(cRef);
scalar x0 = 0.0;
scalar x1 = dx;

eqArray dG0(dG);
dG0 *= -1.0;
eqArray dG1(dG0);

scalar maxDG0 = 0.0;
if (mag(dG1.a()) > maxDG0)
{
    maxDG0 = mag(dG1.a());
    dG0 = eqArray::zero;
    dG0.a() = dG1.a();
    dG0.a() = fmin(maxDer, dG0.a());
    dG0.a() = fmax(-maxDer, dG0.a());
}

if (mag(dG1.b()) > maxDG0)
{
    maxDG0 = mag(dG1.b());
    dG0 = eqArray::zero;
    dG0.b() = dG1.b();
}

if (mag(dG1.c()) > maxDG0)
{
    maxDG0 = mag(dG1.c());
    dG0 = eqArray::zero;
    dG0.c() = dG1.c();
    dG0.c() = fmin(maxDer, dG0.c());
    dG0.c() = fmax(-maxDer, dG0.c());
}

if (mag(dG1.d()) > maxDG0)
{
    maxDG0 = mag(dG1.d());
    dG0 = eqArray::zero;
    dG0.d() = dG1.d();
    dG0.d() = fmin(maxDer, dG0.d());
    dG0.d() = fmax(-maxDer, dG0.d());
}

if (mag(dG1.e()) > maxDG0)
{
    maxDG0 = mag(dG1.e());
    dG0 = eqArray::zero;
    dG0.e() = dG1.e();
}

scalar Y0 = Gsum(nPoints, Y, IDEA, Tlow, Thigh);

IDEA.coeffs() = cRef + dG0*x1;
scalar Y1 = Gsum(nPoints, Y, IDEA, Tlow, Thigh);

scalar x2 = x1 + gold*(x1 - x0);
IDEA.coeffs() = cRef + dG0*x2;
scalar Y2 = Gsum(nPoints, Y, IDEA, Tlow, Thigh);

//cout << "Y0 = " << Y0 << ", Y1 = " << Y1 << ", Y2 = " << Y2 << ", dG=" << dG0 << endl;

// bracket minimum
while( (Y0 < Y1) || (Y2 < Y1) )
{
    subIter++;
    
    if (Y2 < Y1)
    {
        // move box one step right
        x0 = x1;
        x1 = x2;
        x2 += gold*(x1 - x0);

        Y0 = Y1;
        Y1 = Y2;

        IDEA.coeffs() = cRef + dG0*x2;
        Y2 = Gsum(nPoints, Y, IDEA, Tlow, Thigh);
    }
    else
    {
        if (Y0 < Y1)
        {
            // move box left
            x2 = x1;
            x1 = x0;
            x0 -= gold*(x2 - x1);

            Y2 = Y1;
            Y1 = Y0;

            IDEA.coeffs() = cRef + dG0*x0;
            Y0 = Gsum(nPoints, Y, IDEA, Tlow, Thigh);
        }
    }
}
/*
cout<< setprecision(8)
    << scientific
<< "x0 = " << x0 << ", x1 = " << x1 << ", x2 = " << x2
<< "\nY0 = " << Y0 << ", Y1 = " << Y1 << ", Y2 = " << Y2 
<< "\ndG=" << dG0 << endl;
    */
scalar dx = mag(x2-x0);

// find minimum (brent's method)
while (dx > dxMax)
{
    subIter2++;

    scalar xc = 0.5*(x0 + x2);
    IDEA.coeffs() = cRef + dG0*xc;
    scalar Yc = Gsum(nPoints, Y, IDEA, Tlow, Thigh);

    if (xc < x1)
    {
        if (Yc < Y1)
        {
            x2 = x1;
            Y2 = Y1;
            x1 = xc;
            Y1 = Yc;
        }
        else
        {
            x0 = xc;
            Y0 = Yc;
        }
    }
    else
    {
        if (Yc < Y1)
        {
            x0 = x1;
            x1 = xc;
            Y0 = Y1;
            Y1 = Yc;
        }
        else
        {
            x2 = xc;
            Y2 = Yc;
        }
    }
    dx = x2 - x0;
    /*
    cout << setprecision(12)
        << scientific
        << "dx = " << dx << ", Y1 = " << Y1 << endl;
        */
}
/*
cout<< endl << setprecision(8)
    << scientific
<< "x0 = " << x0 << ", x1 = " << x1 << ", x2 = " << x2
<< "\nY0 = " << Y0 << ", Y1 = " << Y1 << ", Y2 = " << Y2 
<< "\ndG=" << dG0 << endl;
    */
//return 0;

