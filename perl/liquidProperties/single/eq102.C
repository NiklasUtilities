#include "eq102.H"
#include <iostream.h>
#include <math.h>

eq102::eq102
(
    scalar a,
    scalar b,
    scalar c,
    scalar d,
    scalar e
)
    :
    coeffs_(a, b, c, d, e)
{}

eq102::eq102
(
    const eqArray& a
)
    :
    coeffs_(a)
{}

eq102::eq102(const eq102& e)
    :
    coeffs_(e.coeffs())
{}

eq102::~eq102()
{}

scalar eq102::Y(scalar T) const
{
    scalar T2 = T*T;
    scalar denom = 1.0 + coeffs_.c()/T + coeffs_.d()/T2;
    return coeffs_.a()*pow(T, coeffs_.b())/denom;
}

scalar eq102::dYda(scalar T) const
{
    scalar T2 = T*T;
    scalar denom = 1.0 + coeffs_.c()/T + coeffs_.d()/T2;
    return pow(T, coeffs_.b())/denom;
}

scalar eq102::dYdb(scalar T) const
{
    return Y(T)*log(T);
}

scalar eq102::dYdc(scalar T) const
{
    scalar T2 = T*T;
    scalar denom = 1.0 + coeffs_.c()/T + coeffs_.d()/T2;
    return -coeffs_.a()*pow(T, coeffs_.b())/(T*pow(denom, 2));
}

scalar eq102::dYdd(scalar T) const
{
    scalar T2 = T*T;
    scalar denom = 1.0 + coeffs_.c()/T + coeffs_.d()/T2;
    return -coeffs_.a()*pow(T, coeffs_.b())/(T2*pow(denom, 2));
}

scalar eq102::dYde(scalar T) const
{
    return 0.0;
}

eqArray eq102::grad(scalar T) const
{
    eqArray g(dYda(T), dYdb(T), dYdc(T), dYdd(T), dYde(T));
    return g;
}

scalar eq102::d2a(scalar T) const
{
    return 0.0;
}

scalar eq102::d2b(scalar T) const
{
    eq102 y1(*this);
    eq102 y0(*this);

    y1.coeffs().b() += delta;
    y0.coeffs().b() -= delta;

    return 0.5*(y1.dYdb(T) - y0.dYdb(T))/delta;

}

scalar eq102::d2c(scalar T) const
{
    eq102 y1(*this);
    eq102 y0(*this);

    y1.coeffs().c() += delta;
    y0.coeffs().c() -= delta;

    return 0.5*(y1.dYdc(T) - y0.dYdc(T))/delta;

}

scalar eq102::d2d(scalar T) const
{
    eq102 y1(*this);
    eq102 y0(*this);

    y1.coeffs().d() += delta;
    y0.coeffs().d() -= delta;

    return 0.5*(y1.dYdd(T) - y0.dYdd(T))/delta;

}

scalar eq102::d2e(scalar T) const
{
    eq102 y1(*this);
    eq102 y0(*this);

    y1.coeffs().e() += delta;
    y0.coeffs().e() -= delta;

    return 0.5*(y1.dYde(T) - y0.dYde(T))/delta;

}

eqArray eq102::grad2(scalar T) const
{
    eqArray g(d2a(T), d2b(T), d2c(T), d2d(T), d2e(T));
    return g;
}

ostream& operator<<(ostream& os, const eq102& e)
{
    os << e.coeffs();
    return os;
}

