#ifndef eq105_H
#define eq105_H

#include "types.H"
#include "eqArray.H"
#include <ostream.h>

class eq105
{
    const static scalar delta = 1.0e-5;

    eqArray coeffs_;

public:
    
    // constructor
    eq105
    (
        scalar a,
        scalar b,
        scalar c,
        scalar d,
        scalar e
    );

    eq105(const eqArray& a);
    eq105(const eq105& e);

    // destructor
    ~eq105();

    // access
    inline const eqArray coeffs() const
    {
        return coeffs_;
    }

    inline eqArray& coeffs()
    {
        return coeffs_;
    }

    // functions

    scalar Y(scalar T) const;
    scalar dYda(scalar T) const;
    scalar dYdb(scalar T) const;
    scalar dYdc(scalar T) const;
    scalar dYdd(scalar T) const;
    scalar dYde(scalar T) const;

    eqArray grad(scalar T) const;

    scalar d2a(scalar T) const;
    scalar d2b(scalar T) const;
    scalar d2c(scalar T) const;
    scalar d2d(scalar T) const;
    scalar d2e(scalar T) const;

    eqArray grad2(scalar T) const;

    // operators

    friend ostream& operator<<(ostream& os, const eq105& e);

};

#endif

