#include "eq106.H"
#include <iostream.h>
#include <math.h>

eq106::eq106
(
    scalar Tc,
    scalar a,
    scalar b,
    scalar c,
    scalar d,
    scalar e
)
    :
    Tc_(Tc),
    coeffs_(a, b, c, d, e)
{}

eq106::eq106
(
    const scalar& Tc,
    const eqArray& a
)
    :
    Tc_(Tc),
    coeffs_(a)
{}

eq106::eq106(const eq106& e)
    :
    Tc_(e.Tc()),
    coeffs_(e.coeffs())
{}

eq106::~eq106()
{}

scalar eq106::Y(scalar T) const
{
    scalar Tr = T/Tc_;
    scalar Tr2 = Tr*Tr;
    scalar Tr3 = Tr2*Tr;
    scalar e = coeffs_.b() + coeffs_.c()*Tr + coeffs_.d()*Tr2 + coeffs_.e()*Tr3;
    return coeffs_.a()*pow(1 - Tr, e);
}

scalar eq106::dYda(scalar T) const
{
    scalar Tr = T/Tc_;
    scalar Tr2 = Tr*Tr;
    scalar Tr3 = Tr2*Tr;
    scalar e = coeffs_.b() + coeffs_.c()*Tr + coeffs_.d()*Tr2 + coeffs_.e()*Tr3;
    return pow(1 - Tr, e);
}

scalar eq106::dYdb(scalar T) const
{
    scalar Tr = T/Tc_;
    scalar logTr = log(1 - Tr);
    
    return logTr*Y(T);
}

scalar eq106::dYdc(scalar T) const
{
    scalar Tr = T/Tc_;
    scalar logTr = log(1 - Tr);

    return logTr*Tr*Y(T);
}

scalar eq106::dYdd(scalar T) const
{
    scalar Tr = T/Tc_;
    scalar logTr = log(1 - Tr);

    return logTr*Tr*Tr*Y(T);
}

scalar eq106::dYde(scalar T) const
{
    scalar Tr = T/Tc_;
    scalar logTr = log(1 - Tr);

    return logTr*Tr*Tr*Tr*Y(T);
}

eqArray eq106::grad(scalar T) const
{
    eqArray g(dYda(T), dYdb(T), dYdc(T), dYdd(T), dYde(T));
    return g;
}

scalar eq106::d2a(scalar T) const
{
    return 0.0;
}

scalar eq106::d2b(scalar T) const
{
    eq106 y1(*this);
    eq106 y0(*this);

    y1.coeffs().b() += delta;
    y0.coeffs().b() -= delta;

    return 0.5*(y1.dYdb(T) - y0.dYdb(T))/delta;

}

scalar eq106::d2c(scalar T) const
{
    eq106 y1(*this);
    eq106 y0(*this);

    y1.coeffs().c() += delta;
    y0.coeffs().c() -= delta;

    return 0.5*(y1.dYdc(T) - y0.dYdc(T))/delta;

}

scalar eq106::d2d(scalar T) const
{
    eq106 y1(*this);
    eq106 y0(*this);

    y1.coeffs().d() += delta;
    y0.coeffs().d() -= delta;

    return 0.5*(y1.dYdd(T) - y0.dYdd(T))/delta;

}

scalar eq106::d2e(scalar T) const
{
    eq106 y1(*this);
    eq106 y0(*this);

    y1.coeffs().e() += delta;
    y0.coeffs().e() -= delta;

    return 0.5*(y1.dYde(T) - y0.dYde(T))/delta;

}

eqArray eq106::grad2(scalar T) const
{
    eqArray g(d2a(T), d2b(T), d2c(T), d2d(T), d2e(T));
    return g;
}

ostream& operator<<(ostream& os, const eq106& e)
{
    os << e.coeffs();
    return os;
}

