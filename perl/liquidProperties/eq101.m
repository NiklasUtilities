function y = eq101(T, a, b, c, d, e)
    y = exp(a+b/T + c*log(T) + d*T^e);

