function y = eq105(T, a, b, c, d, e)
    y = a/b^(1+(1-T/c)^d);
