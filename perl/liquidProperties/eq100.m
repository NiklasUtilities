function y = eq100(T, a, b, c, d, e)
    y = a + b*T + c*T^2 + d*T^3 + e*T^4;
