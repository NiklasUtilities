#!/usr/bin/perl

use strict;

# check for filename
if ($#ARGV != 0)
{
  &usage;
}

# declare variables
my $Tstd = 298.15;
my $dhlatstd;

my $fuelname;
my $mw, my $tc, my $pc, my $vc, my $dipm, my $omega, my $hform;
my $eq, my $eqrho, my $eqpsat, my $eqhlat, my $eqcpl, my $eqcpg;
my $eqmul, my $eqmug, my $eqkappal, my $eqkappag, my $eqsigma;

my @keyword = (
				'fuelname',
				'mw',
				'tc',
				'pc',
				'vc',
				'dipm',
				'omega',
				'hform',
				'density',
				'psat',
				'hlat',
				'cpliquid',
				'cpgas',
				'muliquid',
				'mugas',
				'kappaliquid',
				'kappagas',
				'sigma'
			   );

my %keywordRead;

my %numArgs = (
			   100 => 6,
			   101 => 5,
			   102 => 4,
			   103 => 4,
			   104 => 5,
			   105 => 4,
			   106 => 6,
			   107 => 5
			  );
my %numPre = (
				 100 => 0,
				 101 => 0,
				 102 => 0,
				 103 => 0,
				 104 => 0,
				 105 => 0,
				 106 => 1,
				 107 => 0
				);
my %keyValue;

my @input;
my @rho, my @psat, my @hlat, my @cpliquid, my @cpgas, my @mul;
my @mug, my @kappal, my @kappag, my @sigma;
my @enth;

#main routines
&readInput;

&output;
exit;

#**********************************************************************
sub usage {
  print "Hejsan $#ARGV \n";
  print "usage:\n";
  print "\t nsrds2foam.pl <filename>\n";
  print "\t This will convert the input from straight nsrds functions to\n";
  print "\t FOAM input...\n\t Just copy and paste it in.\n";
}

#**********************************************************************
sub readInput {
  my $filename = $ARGV[0];

  for (my $i=0;$i <= $#keyword; $i++) {
	$keywordRead{$keyword[$i]} = 0;
  }


  open(READ, "<" . $filename);
  while(<READ>) {
	split;

	$keywordRead{$_[0]} = 1;
	$keyValue{$_[0]} = $_[1];
	$input[0] = $_[2] * 1.0;
	$input[1] = $_[3] * 1.0;
	$input[2] = $_[4] * 1.0;
	$input[3] = $_[5] * 1.0;
	$input[4] = $_[6] * 1.0;
	$input[5] = $_[7] * 1.0;

	if ($_[0] eq 'mw') {
	  $mw = $_[1];
	}
	elsif ($_[0] eq 'fuelname') {
	  $fuelname = $_[1];
	}

	elsif ($_[0] eq 'tc') {
	  $tc = $_[1];
	}

	elsif ($_[0] eq 'pc') {
	  $pc = $_[1];
	}

	elsif ($_[0] eq 'vc') {
	  $vc = $_[1];
	}

	elsif ($_[0] eq 'dipm') {
	  $dipm = $_[1];
	}

	elsif ($_[0] eq 'omega') {
	  $omega = $_[1];
	}

	elsif ($_[0] eq 'hform') {
	  $hform = $_[1];
	}

	elsif ($_[0] eq 'density') {
	  $eq = $_[1];
	  $eqrho = $eq;
	  &calcCRho;
	  @rho = @input;
	}

	elsif ($_[0] eq 'psat') {
	  $eq = $_[1];
	  $eqpsat = $eq;
	  @psat = @input;
	}

	elsif ($_[0] eq 'hlat') {
	  $eq = $_[1];
	  $eqhlat = $eq;
	  &calcCOthers;
	  @hlat = @input;
	  &calcHLatForm;
	}

	elsif ($_[0] eq 'cpliquid') {
	  $eq = $_[1];
	  $eqcpl = $eq;
	  &calcCOthers;
	  @cpliquid = @input;
	  &calcCH;
	}

	elsif ($_[0] eq 'cpgas') {
	  $eq = $_[1];
	  $eqcpg = $eq;
	  &calcCOthers;
	  @cpgas = @input;
	}

	elsif ($_[0] eq 'muliquid') {
	  $eq = $_[1];
	  $eqmul = $eq;
	  @mul = @input;
	}

	elsif ($_[0] eq 'mugas') {
	  $eq = $_[1];
	  $eqmug = $eq;
	  @mug = @input;
	}

	elsif ($_[0] eq 'kappaliquid') {
	  $eq = $_[1];
	  $eqkappal = $eq;
	  @kappal = @input;
	}

	elsif ($_[0] eq 'kappagas') {
	  $eq = $_[1];
	  $eqkappag = $eq;
	  @kappag = @input;
	}

	elsif ($_[0] eq 'sigma') {
	  $eq = $_[1];
	  $eqsigma = $eq;
	  @sigma = @input;
	}

  }

  for (my $i=0;$i <= $#keyword; $i++) {
	if ( $keywordRead{$keyword[$i]} == 0 ) {
	  print "keyword '$keyword[$i]' has not been read.\n";
	  exit;
	}
  }

  close(READ);
}

#**********************************************************************
sub calcCRho {

  if ($eq == 100) {
	$input[0] *= $mw;
	$input[1] *= $mw;
	$input[2] *= $mw;
	$input[3] *= $mw;
	$input[4] *= $mw;
	$input[5] *= $mw;
  }
  elsif ($eq == 101) {
# No need to do anything
  }
  elsif ($eq == 102) {
	$input[0] = $mw;
  }
  elsif ($eq == 103) {
	$input[0] *= $mw;
	$input[1] *= $mw;
  }
  elsif ($eq == 104) {
	$input[0] *= $mw;
	$input[1] *= $mw;
	$input[2] *= $mw;
	$input[3] *= $mw;
	$input[4] *= $mw;
  }
  elsif ($eq == 105) {
	$input[0] *= $mw;
  }
  elsif ($eq == 106) {
	$input[0] *= $mw;
  }
  elsif ($eq == 107) {
	$input[0] *= $mw;
	$input[1] *= $mw;
	$input[3] *= $mw;
  }
  else {
	print "Equation $eq not implemented.\n";
	exit;
  }
}

#**********************************************************************
sub calcCOthers {
  if ($eq == 100) {
	$input[0] /= $mw;
	$input[1] /= $mw;
	$input[2] /= $mw;
	$input[3] /= $mw;
	$input[4] /= $mw;
	$input[5] /= $mw;
  }
  elsif ($eq == 101) {
# No need to do anything
  }
  elsif ($eq == 102) {
	$input[0] = $mw;
  }
  elsif ($eq == 103) {
	$input[0] /= $mw;
	$input[1] /= $mw;
  }
  elsif ($eq == 104) {
	$input[0] /= $mw;
	$input[1] /= $mw;
	$input[2] /= $mw;
	$input[3] /= $mw;
	$input[4] /= $mw;
  }
  elsif ($eq == 105) {
	$input[0] /= $mw;
  }
  elsif ($eq == 106) {
	$input[0] /= $mw;
  }
  elsif ($eq == 107) {
	$input[0] /= $mw;
	$input[1] /= $mw;
	$input[3] /= $mw;
  }
  else {
	print "Equation $eq not implemented.\n";
	exit;
  }
}

#**********************************************************************
sub calcCH {
  if (
	  ($keywordRead{'cpliquid'} != 1) ||
	  ($keywordRead{'hlat'} != 1)
	 ) {
	print "All properties to calculate the enthalpy have not been read.\n";
	print "hlat needs to be before cpliquid.\n";
	exit;
  }
	
  if ($eq == 100) {
	$enth[5] = $cpliquid[4]/5.0;
	$enth[4] = $cpliquid[3]/4.0;
	$enth[3] = $cpliquid[2]/3.0;
	$enth[2] = $cpliquid[1]/2.0;
	$enth[1] = $cpliquid[0];
	my $sum = (
			   (
				(
				 ( $enth[5]*$Tstd + $enth[4]
				 )*$Tstd + $enth[3]
				)*$Tstd + $enth[2]
			   )*$Tstd + $enth[1]
			  )*$Tstd;
	$enth[0] = $hform/$mw - $sum - $dhlatstd;
  }
  else {
	print "The enthalpy for the liquid is not implemented for eq $eq.\n";
	exit;
  }
}

#**********************************************************************
sub calcHLatForm {
  if ($eqhlat == 106) {
	my $tr = $Tstd/$tc;
	$dhlatstd = $hlat[0]*(1.0-$tr)**($hlat[1] + $hlat[2]*$tr + $hlat[3]*$tr**2 + $hlat[4]*$tr**3);
  }
  else {
	print "Equation for latent heat of evaporation is not implemented yet.\n";
	exit
  }
}
#**********************************************************************
sub output {
  my $i;
  system mkdir $fuelname;

  open (TEMPLATEC, "<" . "t/template.C");
  open (FUELC, ">" . $fuelname . "/" . $fuelname . ".C");
  while (<TEMPLATEC>) {
	s/FUELNAME/$fuelname/g;
	print FUELC $_;

  }
  close (TEMPLATEC);
  close (FUELC);

  open (TEMPLATEH, "<" . "t/template1.H");
  open (FUELH, ">" . $fuelname . "/" . $fuelname . ".H");
  while (<TEMPLATEH>) {
	s/FUELNAME/$fuelname/g;
	print FUELH $_;

  }
  close (TEMPLATEH);

  print FUELH "        NSRDSfunc" . eval{$eqrho - 100} . " rho_;\n";
  print FUELH "        NSRDSfunc" . eval{$eqpsat - 100} . " pv_;\n";
  print FUELH "        NSRDSfunc" . eval{$eqhlat - 100} . " hl_;\n";
  print FUELH "        NSRDSfunc" . eval{$eqcpl - 100} . " cp_;\n";
  print FUELH "        NSRDSfunc" . eval{$eqcpl - 100} . " h_;\n";
  print FUELH "        NSRDSfunc" . eval{$eqcpg - 100} . " cpg_;\n";
  print FUELH "        NSRDSfunc" . eval{$eqmul - 100} . " mu_;\n";
  print FUELH "        NSRDSfunc" . eval{$eqmug - 100} . " mug_;\n";
  print FUELH "        NSRDSfunc" . eval{$eqkappal - 100} . " K_;\n";
  print FUELH "        NSRDSfunc" . eval{$eqkappag - 100} . " Kg_;\n";
  print FUELH "        NSRDSfunc" . eval{$eqsigma - 100} . " sigma_;\n";
  print FUELH "        APIdiffCoefFunc D_;\n";

  open (TEMPLATEH, "<" . "t/template2.H");
  while (<TEMPLATEH>) {
	s/FUELNAME/$fuelname/g;
	print FUELH $_;

  }
  close (TEMPLATEH);

  print FUELH "            liquid($mw, $tc, $pc, $vc, $dipm, $omega),\n";
  print FUELH "            rho_(";
  $eq = $eqrho;
  if ( $numPre{$eq} > 0) {
	print FUELH "$tc, ";
  }
  for ($i=0; $i < $numArgs{$eq} - $numPre{$eq}; $i++) {
	print FUELH $rho[$i];
	if ($i < $numArgs{$eq} - $numPre{$eq} - 1) {
	  print FUELH ", ";
	}
  }
  print FUELH "),\n";
  print FUELH "            pv_(";
  $eq = $eqpsat;
  if ( $numPre{$eq} > 0) {
	print FUELH "$tc, ";
  }
  for ($i=0; $i < $numArgs{$eq} - $numPre{$eq}; $i++) {
	print FUELH $psat[$i];
	if ($i < $numArgs{$eq} - $numPre{$eq} - 1) {
	  print FUELH ", ";
	}
  }
  print FUELH "),\n";
  print FUELH "            hl_(";
  $eq = $eqhlat;
  if ( $numPre{$eq} > 0) {
	print FUELH "$tc, ";
  }
  for ($i=0; $i < $numArgs{$eq} - $numPre{$eq}; $i++) {
	print FUELH $hlat[$i];
	if ($i < $numArgs{$eq} - $numPre{$eq} - 1) {
	  print FUELH ", ";
	}
  }
  print FUELH "),\n";
  print FUELH "            cp_(";
  $eq = $eqcpl;
  if ( $numPre{$eq} > 0) {
	print FUELH "$tc, ";
  }
  for ($i=0; $i < $numArgs{$eq} - $numPre{$eq}; $i++) {
	print FUELH $cpliquid[$i];
	if ($i < $numArgs{$eq} - $numPre{$eq} - 1) {
	  print FUELH ", ";
	}
  }

  print FUELH "),\n";
  print FUELH "            // NN: enthalpy, h_, is not used in the sprayModel.\n";
  print FUELH "            // For consistency, the enthalpy is derived from hlat and hl.\n";
  print FUELH "            // It is, however, convenient to have it available.\n";
  print FUELH "            h_(";
  if ( $numPre{$eq} > 0) {
	print FUELH "$tc, ";
  }
  for ($i=0; $i < $numArgs{$eq} - $numPre{$eq}; $i++) {
	print FUELH $enth[$i];
	if ($i < $numArgs{$eq} - $numPre{$eq} - 1) {
	  print FUELH ", ";
	}
  }

  print FUELH "),\n";
  print FUELH "            cpg_(";
  $eq = $eqcpg;
  if ( $numPre{$eq} > 0) {
	print FUELH "$tc, ";
  }
  for ($i=0; $i < $numArgs{$eq} - $numPre{$eq}; $i++) {
	print FUELH $cpgas[$i];
	if ($i < $numArgs{$eq} - $numPre{$eq} - 1) {
	  print FUELH ", ";
	}
  }
  print FUELH "),\n";
  print FUELH "            mu_(";
  $eq = $eqmul;
  if ( $numPre{$eq} > 0) {
	print FUELH "$tc, ";
  }
  for ($i=0; $i < $numArgs{$eq} - $numPre{$eq}; $i++) {
	print FUELH $mul[$i];
	if ($i < $numArgs{$eq} - $numPre{$eq} - 1) {
	  print FUELH ", ";
	}
  }
  print FUELH "),\n";
  print FUELH "            mug_(";
  $eq = $eqmug;
  if ( $numPre{$eq} > 0) {
	print FUELH "$tc, ";
  }
  for ($i=0; $i < $numArgs{$eq} - $numPre{$eq}; $i++) {
	print FUELH $mug[$i];
	if ($i < $numArgs{$eq} - $numPre{$eq} - 1) {
	  print FUELH ", ";
	}
  }

  print FUELH "),\n";
  print FUELH "            K_(";
  $eq = $eqkappal;
  if ( $numPre{$eq} > 0) {
	print FUELH "$tc, ";
  }
  for ($i=0; $i < $numArgs{$eq} - $numPre{$eq}; $i++) {
	print FUELH $kappal[$i];
	if ($i < $numArgs{$eq} - $numPre{$eq} - 1) {
	  print FUELH ", ";
	}
  }

  print FUELH "),\n";
  print FUELH "            Kg_(";
  $eq = $eqkappag;
  if ( $numPre{$eq} > 0) {
	print FUELH "$tc, ";
  }
  for ($i=0; $i < $numArgs{$eq} - $numPre{$eq}; $i++) {
	print FUELH $kappag[$i];
	if ($i < $numArgs{$eq} - $numPre{$eq} - 1) {
	  print FUELH ", ";
	}
  }

  print FUELH "),\n";
  print FUELH "            sigma_(";
  $eq = $eqsigma;
  if ( $numPre{$eq} > 0) {
	print FUELH "$tc, ";
  }
  for ($i=0; $i < $numArgs{$eq} - $numPre{$eq}; $i++) {
	print FUELH $sigma[$i];
	if ($i < $numArgs{$eq} - $numPre{$eq} - 1) {
	  print FUELH ", ";
	}
  }

  print FUELH "),\n";
  print FUELH "            D_(147.18, 20.1, $mw, 28) // NN: Same as nHeptane\n";
  
  print FUELH "        {}\n";
  print FUELH "        $fuelname\n";
  print FUELH "        (\n";
  print FUELH "            const liquid& l,\n";
  print FUELH "            const NSRDSfunc" . eval{$eqrho - 100} . "& density,\n";
  print FUELH "            const NSRDSfunc" . eval{$eqpsat - 100} . "& vapourPressure,\n";
  print FUELH "            const NSRDSfunc" . eval{$eqhlat - 100} . "& heatOfVapourisation,\n";
  print FUELH "            const NSRDSfunc" . eval{$eqcpl - 100} . "& heatCapacity,\n";
  print FUELH "            const NSRDSfunc" . eval{$eqcpl - 100} . "& enthalpy,\n";
  print FUELH "            const NSRDSfunc" . eval{$eqcpg - 100} . "& idealGasHeatCapacity,\n";
  print FUELH "            const NSRDSfunc" . eval{$eqmul - 100} . "& dynamicViscosity,\n";
  print FUELH "            const NSRDSfunc" . eval{$eqmug - 100} . "& vapourDynamicViscosity,\n";
  print FUELH "            const NSRDSfunc" . eval{$eqkappal - 100} . "& thermalConductivity,\n";
  print FUELH "            const NSRDSfunc" . eval{$eqkappag - 100} . "& vapourThermalConductivity,\n";
  print FUELH "            const NSRDSfunc" . eval{$eqsigma - 100} . "& surfaceTension,\n";
  print FUELH "            const APIdiffCoefFunc& vapourDiffussivity\n";
  print FUELH "        )\n";

  open (TEMPLATEH, "<" . "t/template3.H");
  while (<TEMPLATEH>) {
	s/FUELNAME/$fuelname/g;
	print FUELH $_;

  }

  close(FUELH);
  close(TEMPLATEH);
  print "Wrote files '$fuelname.C' and '$fuelname.H' to directory '$fuelname'.\n";
}
