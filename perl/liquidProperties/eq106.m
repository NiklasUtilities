function y = eq106(Tr, a, b, c, d, e)
    y = a*(1.0 - Tr)^(b + c*Tr + d*Tr*Tr + e*Tr*Tr*Tr);
